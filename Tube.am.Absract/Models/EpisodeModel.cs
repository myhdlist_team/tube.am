﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tube.am.Absract.Models
{
    public class EpisodeModel
    {
        public int Id { get; set; }

        public int? SeasonIndex { get; set; }

        public int EpisodeIndex { get; set; }

        public string PosterLink { get; set; }

        public string PosterAlt { get; set; }

        public DateTime ReleaseDate { get; set; }

        public List<PersonModel> Actors { get; set; }

        public List<PersonModel> Creators { get; set; }

        public VideoModel Video { get; set; }
    }
}
