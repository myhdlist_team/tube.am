﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tube.am.Absract.Models
{
    public class MovieModel
    {
        public int Id { get; set; }

        public DateTime ReleaseDate { get; set; }

        public string PosterLink { get; set; }

        public string PosterAlt { get; set; }

        public List<PersonModel> Actors { get; set; }

        public List<PersonModel> Creators { get; set; }

        public VideoModel Video { get; set; }
    }
}
