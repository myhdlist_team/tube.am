﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tube.am.Absract.Models
{
    public class NewsModel
    {
        public int Id { get; set; }

        public DateTime ReleaseDate { get; set; }

        public VideoModel Video { get; set; }
    }
}
