﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tube.am.Absract.Models
{
    public class PlaylistModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime CreatedDate { get; set; }

        public int ViewCount { get; set; }

        public List<VideoModel> Videos { get; set; }

        public UserModel Channel { get; set; }

        public PlaylistModel()
        {
            Videos = new List<VideoModel>();
            Channel = new UserModel();
        }
    }
}
