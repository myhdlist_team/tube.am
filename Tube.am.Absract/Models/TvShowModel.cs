﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tube.am.Absract.Models
{
    public class TvShowModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string PosterLink { get; set; }

        public string PosterAlt { get; set; }

        public DateTime ReleaseDate { get; set; }

        public DateTime? EndDate { get; set; }

        public List<PersonModel> Actors { get; set; }

        public List<PersonModel> Creators { get; set; }

        public List<EpisodeModel> Episodes { get; set; } // TODO: may probably get episode ids , not full episode

        public TvShowModel()
        {
            Episodes = new List<EpisodeModel>();
        }
    }
}
