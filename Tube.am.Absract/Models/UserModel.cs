﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tube.am.Absract.Models
{
    public class UserModel
    {
        public string Id { get; set; }

        public string NameSurname { get; set; }

        public string Username { get; set; }

        public string ChannelName { get; set; }

        public string Email { get; set; }

        public bool EmailConfirmed { get; set; }

        public string PasswordHash { get; set; }

        public DateTime? RegistrationDate { get; set; }

        public DateTime? LastLogoutDate { get; set; }

        public string ChannelImageLink { get; set; }

        public string ChannelImageAlt { get; set; }

        public string ChannelCoverImageLink { get; set; }

        public string ChannelCoverImageAlt { get; set; }
    }
}
