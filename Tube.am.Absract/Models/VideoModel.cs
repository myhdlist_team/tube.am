﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tube.am.Absract.Models
{
    public class VideoModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Link { get; set; }

        public DateTime UploadedDate { get; set; }

        public TimeSpan Duration { get; set; }

        public string ThumbnailImageLink { get; set; }

        public string ThumbnailImageAlt { get; set; }

        public int ViewCount { get; set; }

        public int LikesCount { get; set; }

        public int DislikesCount { get; set; }

        public string SearchTerm { get; set; }

        public bool Is360 { get; set; }

        public bool Is480 { get; set; }

        public bool Is720 { get; set; }

        public string Type { get; set; }

        public UserModel Channel { get; set; }

        public List<CommentModel> Comments { get; set; }

        public List<string> Screenshots { get; set; }

        public VideoModel()
        {
            Screenshots = new List<String>();
        }

    }
}
