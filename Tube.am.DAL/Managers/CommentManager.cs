﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tube.am.Absract.Models;
using Tube.am.DB;

namespace Tube.am.DAL.Managers
{
    public class CommentManager
    {
        public static CommentModel GetCommentById(DbDataContext db, int id)
        {
            CommentModel retItem = new CommentModel();

            Comment tmpItem = db.Comments.SingleOrDefault(x => x.Id == id);

            retItem.Id = tmpItem.Id;
            retItem.Date = tmpItem.Date;
            retItem.Text = tmpItem.Text;

            var query = from q in db.CommentVotes where q.CommentId == id select q;

            if (query == null || !query.Any())
            {
                retItem.LikesCount = 0;
                retItem.DislikesCount = 0;
            }
            
            retItem.LikesCount = query.Count(x => x.IsLiked);
            retItem.DislikesCount = query.Count(x => !x.IsLiked);

            return retItem;
        }

        /// <summary>
        /// Get comments for Video
        /// </summary>
        /// <param name="db">Data context</param>
        /// <param name="videoId">Video id to get comments for</param>
        /// <returns></returns>
        public static List<CommentModel> GetCommentsByVideo(DbDataContext db, int videoId)
        {
            List<CommentModel> retItems = new List<CommentModel>();

            var query = from q in db.Comments where q.VideoId == videoId select q;

            if (query == null || !query.Any())
                return retItems;

            foreach (var item in query)
            {
                retItems.Add(GetCommentById(db, item.Id));
            }

            return retItems;
        }

        /// <summary>
        /// Add comment to Video
        /// </summary>
        /// <param name="videoId">Video id to add a comment to</param>
        /// <param name="userId">User id who is commenting under video</param>
        /// <param name="text">Comment text</param>
        public static void CommentUnderVideo(int videoId, string userId, string text)
        {
            using (DbDataContext db = new DbDataContext())
            {
                Comment tmpComment = new Comment();

                tmpComment.Date = DateTime.Now;
                tmpComment.Text = text;
                tmpComment.VideoId = videoId;
                tmpComment.UserId = userId;

                db.Comments.InsertOnSubmit(tmpComment);
                db.SubmitChanges();
            }
        }

        /// <summary>
        /// Like or Dislike comment by user
        /// </summary>
        /// <param name="commentId">Comment id which is liked or disliked</param>
        /// <param name="userId">User id who is liking or disliking comment</param>
        /// <param name="isLike">True to like and False to dislike comment</param>
        public static void LikeDislikeComment(int commentId, string userId, bool isLike)
        {
            using (DbDataContext db = new DbDataContext())
            {
                CommentVote tmpVote = db.CommentVotes.SingleOrDefault(x => x.CommentId == commentId && x.UserId == userId);

                if (tmpVote == null)
                {
                    if (tmpVote.IsLiked != isLike)
                    {
                        tmpVote.IsLiked = isLike;

                        db.SubmitChanges();
                    }
                }
                else
                {
                    tmpVote = new CommentVote();

                    tmpVote.CommentId = commentId;
                    tmpVote.UserId = userId;
                    tmpVote.IsLiked = isLike;

                    db.CommentVotes.InsertOnSubmit(tmpVote);
                    db.SubmitChanges();
                }
            }
        }
    }
}
