﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tube.am.Absract.Models;
using Tube.am.DB;

namespace Tube.am.DAL.Managers
{
    public class EpisodeManager
    {
        public static EpisodeModel GetEpisodeById(DbDataContext db, int id)
        {
            EpisodeModel retItem = new EpisodeModel();

            Episode tmpItem = db.Episodes.SingleOrDefault(x => x.Id == id);

            if (tmpItem == null)
                return retItem;

            retItem.Id = tmpItem.Id;
            retItem.ReleaseDate = tmpItem.ReleaseDate;
            retItem.SeasonIndex = tmpItem.SeasonIndex;
            retItem.EpisodeIndex = tmpItem.EpisodeIndex;
            retItem.PosterLink = tmpItem.PosterLink;
            retItem.PosterAlt = tmpItem.PosterAlt;
            retItem.Actors = PersonManager.GetActorsForEpisode(db, id);
            retItem.Creators = PersonManager.GetCreatorsForEpisode(db, id);
            retItem.Video = VideoManager.GetVideoById(db, tmpItem.VideoId);

            return retItem;
        }


        public static List<EpisodeModel> GetEpisodesByTvShowId(int showId, int seasonIndex = 0)
        {
            List<EpisodeModel> retItems = new List<EpisodeModel>();

            using (DbDataContext db = new DbDataContext())
            {
                var query = from q in db.Episodes where q.TvShowId == showId select q;

                if (query == null || !query.Any())
                    return retItems;

                if (seasonIndex != 0)
                    query = query.Where(x => x.SeasonIndex == seasonIndex);

                foreach (var item in query)
                {
                    retItems.Add(GetEpisodeById(db, item.Id));
                }
            }

            return retItems;
        }

        public static int AddEpisodeToTvShow(DbDataContext db, EpisodeModel episode, int tvShowId, string userId = null)
        {
            int retId;

            Episode tmpItem = new Episode();

            tmpItem.EpisodeIndex = episode.EpisodeIndex;
            tmpItem.SeasonIndex = episode.SeasonIndex;
            tmpItem.ReleaseDate = episode.ReleaseDate;
            tmpItem.TvShowId = tvShowId;

            db.Episodes.InsertOnSubmit(tmpItem);
            db.SubmitChanges();

            foreach (var item in episode.Actors)
            {
                Person tmpPerson = db.Persons.SingleOrDefault(x => x.NameSurname == item.NameSurname);

                if (tmpPerson == null)
                {
                    tmpPerson = new Person(); ;

                    tmpPerson.NameSurname = item.NameSurname;

                    db.Persons.InsertOnSubmit(tmpPerson);
                    db.SubmitChanges();
                }

                PersonRoleMovieTvShowEpisodeMapping tmpMap = new PersonRoleMovieTvShowEpisodeMapping();

                tmpMap.EpisodeId = tmpItem.Id;
                tmpMap.PersonId = tmpPerson.Id;
                tmpMap.Role = 1;

                db.PersonRoleMovieTvShowEpisodeMappings.InsertOnSubmit(tmpMap);
                db.SubmitChanges();
            }

            foreach (var item in episode.Creators)
            {
                Person tmpPerson = db.Persons.SingleOrDefault(x => x.NameSurname == item.NameSurname);

                if (tmpPerson == null)
                {
                    tmpPerson = new Person();

                    tmpPerson.NameSurname = item.NameSurname;

                    db.Persons.InsertOnSubmit(tmpPerson);
                    db.SubmitChanges();
                }

                PersonRoleMovieTvShowEpisodeMapping tmpMap = new PersonRoleMovieTvShowEpisodeMapping();

                tmpMap.EpisodeId = tmpItem.Id;
                tmpMap.PersonId = tmpPerson.Id;
                tmpMap.Role = 2;

                db.PersonRoleMovieTvShowEpisodeMappings.InsertOnSubmit(tmpMap);
                db.SubmitChanges();
            }

            int videoId = 0;

            if (String.IsNullOrEmpty(userId))
                videoId = VideoManager.AddVideoItem(episode.Video);

            Video tmpVideo = db.Videos.SingleOrDefault(x => x.Id == videoId);

            tmpVideo.Type = "episode";

            db.SubmitChanges();

            tmpItem.VideoId = videoId;

            db.SubmitChanges();

            retId = tmpItem.Id;

            return retId;
        }
    }
}
