﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tube.am.Absract.Models;
using Tube.am.DB;

namespace Tube.am.DAL.Managers
{
    public class MovieManager
    {
        public static MovieModel GetMovieById(DbDataContext db, int id)
        {
            MovieModel retItem = new MovieModel();

            Movie tmpItem = db.Movies.SingleOrDefault(x => x.Id == id);

            if (tmpItem == null)
                return retItem;

            retItem.Id = tmpItem.Id;
            retItem.ReleaseDate = tmpItem.ReleaseDate;
            retItem.PosterLink = tmpItem.PosterLink;
            retItem.PosterAlt = tmpItem.PosterAlt;
            retItem.Actors = PersonManager.GetActorsForMovie(db, id);
            retItem.Creators = PersonManager.GetCreatorsForMovie(db, id);
            retItem.Video = VideoManager.GetVideoById(db, tmpItem.VideoId);

            return retItem;
        }

        public static List<MovieModel> GetUsersAddedMovies(string userId)
        {
            List<MovieModel> retItems = new List<MovieModel>();

            using (DbDataContext db = new DbDataContext())
            {
                var query = from q in db.Videos where q.Type == "movie" && q.UserId == userId join t in db.Movies on q.Id equals t.VideoId select t;

                if (query == null || !query.Any())
                    return retItems;

                foreach (var item in query)
                {
                    retItems.Add(GetMovieById(db, item.Id));
                }
            }

            return retItems;
        }

        public static List<MovieModel> GetMostViewedMovies()
        {
            List<MovieModel> retItem = new List<MovieModel>();

            using (DbDataContext db = new DbDataContext())
            {
                var query = from q in db.Videos where q.Type == "movie" join t in db.Movies on q.Id equals t.VideoId select new { movie = t, viewCount = q.ViewCount };

                if (query == null && !query.Any())
                    return retItem;

                query = query.OrderByDescending(x => x.viewCount).Take(6); //Temp number

                foreach (var item in query)
                {
                    retItem.Add(GetMovieById(db, item.movie.Id));
                }
            }

            return retItem;
        }

        public static List<MovieModel> GetNewestMovies()
        {
            List<MovieModel> retItem = new List<MovieModel>();

            using (DbDataContext db = new DbDataContext())
            {
                var query = from q in db.Videos where q.Type == "movie" join t in db.Movies on q.Id equals t.VideoId select new { movie = t, date = q.UploadDate };

                if (query == null || !query.Any())
                    return retItem;

                query = query.OrderByDescending(x => x.date).Take(6);

                foreach (var item in query)
                {
                    retItem.Add(GetMovieById(db, item.movie.Id));
                }
            }

            return retItem;
        }

        public static int AddMovie(MovieModel movie, string userId = null)
        {
            int retId;

            using (DbDataContext db = new DbDataContext())
            {
                Movie tmpItem = new Movie();

                tmpItem.ReleaseDate = movie.ReleaseDate;

                db.Movies.InsertOnSubmit(tmpItem);
                db.SubmitChanges();

                foreach (var item in movie.Actors)
                {
                    Person tmpPerson = db.Persons.SingleOrDefault(x => x.NameSurname == item.NameSurname);

                    if (tmpPerson == null)
                    {
                        tmpPerson = new Person();

                        tmpPerson.NameSurname = item.NameSurname;

                        db.Persons.InsertOnSubmit(tmpPerson);
                        db.SubmitChanges();
                    }

                    PersonRoleMovieTvShowEpisodeMapping tmpMap = new PersonRoleMovieTvShowEpisodeMapping();

                    tmpMap.MovieId = tmpItem.Id;
                    tmpMap.PersonId = tmpPerson.Id;
                    tmpMap.Role = 1;

                    db.PersonRoleMovieTvShowEpisodeMappings.InsertOnSubmit(tmpMap);
                    db.SubmitChanges();
                }

                foreach (var item in movie.Creators)
                {
                    Person tmpPerson = db.Persons.SingleOrDefault(x => x.NameSurname == item.NameSurname);

                    if (tmpPerson == null)
                    {
                        tmpPerson = new Person();

                        tmpPerson.NameSurname = item.NameSurname;

                        db.Persons.InsertOnSubmit(tmpPerson);
                        db.SubmitChanges();
                    }

                    PersonRoleMovieTvShowEpisodeMapping tmpMap = new PersonRoleMovieTvShowEpisodeMapping();

                    tmpMap.MovieId = tmpItem.Id;
                    tmpMap.PersonId = tmpPerson.Id;
                    tmpMap.Role = 2;

                    db.PersonRoleMovieTvShowEpisodeMappings.InsertOnSubmit(tmpMap);
                    db.SubmitChanges();
                }

                int videoId = 0;

                if (String.IsNullOrEmpty(userId))
                    videoId = VideoManager.AddVideoItem(movie.Video);

                Video tmpVideo = db.Videos.SingleOrDefault(x => x.Id == videoId);

                tmpVideo.Type = "movie";

                db.SubmitChanges();

                tmpItem.VideoId = videoId;

                db.SubmitChanges();

                retId = tmpItem.Id;
            }

            return retId;
        }
    }
}
