﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tube.am.Absract.Models;
using Tube.am.DB;

namespace Tube.am.DAL.Managers
{
    public class NewsManager
    {
        public static NewsModel GetNewsById(DbDataContext db, int id)
        {
            NewsModel retItem = new NewsModel();

            New tmpItem = db.News.SingleOrDefault(x => x.Id == id);

            if (tmpItem == null)
                return retItem;

            retItem.Id = tmpItem.Id;
            retItem.ReleaseDate = tmpItem.ReleaseDate;
            retItem.Video = VideoManager.GetVideoById(db, tmpItem.VideoId);

            return retItem;
        }

        public static List<NewsModel> GetNewestNews()
        {
            List<NewsModel> retItem = new List<NewsModel>();

            using (DbDataContext db = new DbDataContext())
            {
                var query = from q in db.Videos where q.Type == "news" join t in db.News on q.Id equals t.VideoId select new { news = t, date = q.UploadDate };

                if (query == null || !query.Any())
                    return retItem;

                query = query.OrderByDescending(x => x.date).Take(6);

                foreach (var item in query)
                {
                    retItem.Add(GetNewsById(db, item.news.Id));
                }
            }

            return retItem;
        }

        public static int AddNews(NewsModel news, string userId = null)
        {
            int retId;

            using (DbDataContext db = new DbDataContext())
            {
                New tmpItem = new New();

                tmpItem.ReleaseDate = news.ReleaseDate;

                db.News.InsertOnSubmit(tmpItem);
                db.SubmitChanges();

                int videoId = 0;

                if (String.IsNullOrEmpty(userId))
                    videoId = VideoManager.AddVideoItem(news.Video);

                Video tmpVideo = db.Videos.SingleOrDefault(x => x.Id == videoId);

                tmpVideo.Type = "news";

                db.SubmitChanges();

                tmpItem.VideoId = videoId;

                db.SubmitChanges();

                retId = tmpItem.Id;
            }

            return retId;
        }

    }
}
