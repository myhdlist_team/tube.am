﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tube.am.Absract.Models;
using Tube.am.DB;

namespace Tube.am.DAL.Managers
{
    public class PersonManager
    {
        public static PersonModel GetPersonById(DbDataContext db, int id)
        {
            PersonModel retItem = new PersonModel();

            Person tmpItem = db.Persons.SingleOrDefault(x => x.Id == id);

            if (tmpItem == null)
                return retItem;

            retItem.Id = tmpItem.Id;
            retItem.NameSurname = tmpItem.NameSurname;

            return retItem;
        }

        #region Getting Movie Cast

        public static List<PersonModel> GetActorsForMovie(DbDataContext db, int movieId)
        {
            List<PersonModel> retItems = new List<PersonModel>();

            var query = from q in db.PersonRoleMovieTvShowEpisodeMappings where q.MovieId != null && q.MovieId == movieId select q;

            if (query == null || !query.Any())
                return retItems;

            query = query.Where(x => x.Role == 1);

            foreach (var item in query)
            {
                retItems.Add(GetPersonById(db, item.PersonId));
            }

            return retItems;
        }
        public static List<PersonModel> GetCreatorsForMovie(DbDataContext db, int movieId)
        {
            List<PersonModel> retItems = new List<PersonModel>();

            var query = from q in db.PersonRoleMovieTvShowEpisodeMappings where q.MovieId != null && q.MovieId == movieId select q;

            if (query == null || !query.Any())
                return retItems;

            query = query.Where(x => x.Role == 2);

            foreach (var item in query)
            {
                retItems.Add(GetPersonById(db, item.PersonId));
            }

            return retItems;
        }

        #endregion

        #region Getting TvShow Cast

        public static List<PersonModel> GetActorsForTvShow(DbDataContext db, int showId)
        {
            List<PersonModel> retItems = new List<PersonModel>();

            var query = from q in db.PersonRoleMovieTvShowEpisodeMappings where q.TvShowId != null && q.TvShowId == showId select q;

            if (query == null || !query.Any())
                return retItems;

            query = query.Where(x => x.Role == 1);

            foreach (var item in query)
            {
                retItems.Add(GetPersonById(db, item.PersonId));
            }

            return retItems;
        }
        public static List<PersonModel> GetCreatorsForTvShow(DbDataContext db, int showId)
        {
            List<PersonModel> retItems = new List<PersonModel>();

            var query = from q in db.PersonRoleMovieTvShowEpisodeMappings where q.TvShowId != null && q.TvShowId == showId select q;

            if (query == null || !query.Any())
                return retItems;

            query = query.Where(x => x.Role == 2);

            foreach (var item in query)
            {
                retItems.Add(GetPersonById(db, item.PersonId));
            }

            return retItems;
        }

        #endregion

        #region Getting Movie Cast

        public static List<PersonModel> GetActorsForEpisode(DbDataContext db, int episodeId)
        {
            List<PersonModel> retItems = new List<PersonModel>();

            var query = from q in db.PersonRoleMovieTvShowEpisodeMappings where q.EpisodeId != null && q.EpisodeId == episodeId select q;

            if (query == null || !query.Any())
                return retItems;

            query = query.Where(x => x.Role == 1);

            foreach (var item in query)
            {
                retItems.Add(GetPersonById(db, item.PersonId));
            }

            return retItems;
        }
        public static List<PersonModel> GetCreatorsForEpisode(DbDataContext db, int episodeId)
        {
            List<PersonModel> retItems = new List<PersonModel>();

            var query = from q in db.PersonRoleMovieTvShowEpisodeMappings where q.EpisodeId != null && q.EpisodeId == episodeId select q;

            if (query == null || !query.Any())
                return retItems;

            query = query.Where(x => x.Role == 2);

            foreach (var item in query)
            {
                retItems.Add(GetPersonById(db, item.PersonId));
            }

            return retItems;
        }

        #endregion
    }
}
