﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tube.am.Absract.Models;
using Tube.am.DB;

namespace Tube.am.DAL.Managers
{
    public class PlaylistManager
    {
        /// <summary>
        /// Getting Playlist model from database by id
        /// </summary>
        /// <param name="db">DataBase data context</param>
        /// <param name="id">Playlist id</param>
        /// <returns></returns>
        public static PlaylistModel GetPlaylistById(DbDataContext db, int id)
        {
            PlaylistModel retItem = new PlaylistModel();

            Playlist tmpPlaylist = db.Playlists.SingleOrDefault(x => x.Id == id);

            retItem.Id = tmpPlaylist.Id;
            retItem.Name = tmpPlaylist.Name;
            retItem.Description = tmpPlaylist.Description;
            retItem.CreatedDate = tmpPlaylist.CreatedDate;
            retItem.ViewCount = tmpPlaylist.ViewCount;
            retItem.Videos = VideoManager.GetPlaylistVideos(db, id);
            retItem.Channel = UserManager.GetUserById(tmpPlaylist.UserId);

            return retItem;
        }

        public static PlaylistModel GetPlaylistById(int id)
        {
            PlaylistModel model = new PlaylistModel();

            using (DbDataContext db = new DbDataContext())
            {
                model = GetPlaylistById(db, id);
            }

            return model;
        }

        /// <summary>
        /// Get top 4 popular playlists
        /// </summary>
        /// <returns></returns>
        public static List<PlaylistModel> GetPopularPlaylists()
        {
            List<PlaylistModel> retItems = new List<PlaylistModel>();

            using (DbDataContext db = new DbDataContext())
            {
                var query = from q in db.Playlists select q;

                if (query == null || !query.Any())
                    return retItems;

                query = query.OrderByDescending(x => x.ViewCount).Take(4);

                foreach (var item in query)
                {
                    retItems.Add(GetPlaylistById(db, item.Id));
                }
            }

            return retItems;
        }

        /// <summary>
        /// Get all playlists of user by user Id
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <returns></returns>
        public static List<PlaylistModel> GetUserPlaylists(string userId)
        {
            List<PlaylistModel> retItems = new List<PlaylistModel>();

            using (DbDataContext db = new DbDataContext())
            {
                var query = from q in db.Playlists where q.UserId == userId select q;

                if (query == null || !query.Any())
                    return retItems;

                foreach (var item in query)
                {
                    retItems.Add(GetPlaylistById(db, item.Id));
                }
            }

            return retItems;
        }

        /// <summary>
        /// Add playlist for user(description is optional)
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <param name="name">Playlist name</param>
        /// <param name="description">Playlist description(ooptional)</param>
        public static void AddPlaylistForUser(string userId, string name, int videoId = 0, string description = null)
        {
            using (DbDataContext db = new DbDataContext())
            {
                Playlist tmpItem = new Playlist();

                tmpItem.Name = name;
                tmpItem.CreatedDate = DateTime.Now;
                tmpItem.ViewCount = 0;
                if (!String.IsNullOrEmpty(description))
                    tmpItem.Description = description;

                tmpItem.UserId = userId;

                db.Playlists.InsertOnSubmit(tmpItem);
                db.SubmitChanges();

                if (videoId != 0)
                {
                    PlaylistVideoMapping tmpMap = new PlaylistVideoMapping();

                    tmpMap.VideoId = videoId;
                    tmpMap.PlaylistId = tmpItem.Id;

                    db.PlaylistVideoMappings.InsertOnSubmit(tmpMap);
                    db.SubmitChanges();
                }
            }

            return;
        }

        /// <summary>
        /// Update playlist information
        /// </summary>
        /// <param name="id">Playlist Id</param>
        /// <param name="name">Playlist name</param>
        /// <param name="description">Playlist description</param>
        public static void UpdatePlaylistInfo(int id, string name = null, string description = null)
        {
            using (DbDataContext db = new DbDataContext())
            {
                Playlist tmpItem = db.Playlists.SingleOrDefault(x => x.Id == id);

                if (tmpItem == null)
                    return;

                if (!String.IsNullOrEmpty(name))
                    tmpItem.Name = name;
                if (!String.IsNullOrEmpty(description))
                    tmpItem.Description = description;

                db.SubmitChanges();
            }

            return;
        }

        public static void AddPlaylistView(int id)
        {
            using (DbDataContext db = new DbDataContext())
            {
                Playlist tmpPl = db.Playlists.SingleOrDefault(x => x.Id == id);

                if (tmpPl == null)
                    return;

                tmpPl.ViewCount = tmpPl.ViewCount + 1;

                db.SubmitChanges();
            }
        }
    }
}
