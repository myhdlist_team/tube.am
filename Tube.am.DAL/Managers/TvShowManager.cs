﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tube.am.Absract.Models;
using Tube.am.DB;

namespace Tube.am.DAL.Managers
{
    public class TvShowManager
    {
        public static TvShowModel GetTvShowById(DbDataContext db, int id)
        {
            TvShowModel retItem = new TvShowModel();

            TvShow tmpItem = db.TvShows.SingleOrDefault(x => x.Id == id);

            if (tmpItem == null)
                return retItem;

            retItem.Id = tmpItem.Id;
            retItem.Name = tmpItem.Name;
            retItem.Description = tmpItem.Description;
            retItem.EndDate = tmpItem.EndDate;
            retItem.ReleaseDate = tmpItem.ReleaseDate;
            retItem.PosterLink = tmpItem.PosterLink;
            retItem.PosterAlt = tmpItem.PosterAlt;
            retItem.Actors = PersonManager.GetActorsForTvShow(db, id);
            retItem.Creators = PersonManager.GetCreatorsForTvShow(db, id);
            retItem.Episodes = EpisodeManager.GetEpisodesByTvShowId(id);

            return retItem;
        }


        public static int AddTvShow(TvShowModel show, string userId = null)
        {
            int retId;

            using (DbDataContext db = new DbDataContext())
            {
                TvShow tmpItem = new TvShow();

                tmpItem.Description = show.Description;
                tmpItem.Name = show.Name;
                tmpItem.EndDate = show.EndDate;
                tmpItem.ReleaseDate = show.ReleaseDate;

                db.TvShows.InsertOnSubmit(tmpItem);
                db.SubmitChanges();

                foreach (var item in show.Episodes)
                {
                    EpisodeManager.AddEpisodeToTvShow(db, item, tmpItem.Id, userId);
                }

                foreach (var item in show.Actors)
                {
                    Person tmpPerson = db.Persons.SingleOrDefault(x => x.NameSurname == item.NameSurname);

                    if (tmpPerson == null)
                    {
                        tmpPerson = new Person(); ;

                        tmpPerson.NameSurname = item.NameSurname;

                        db.Persons.InsertOnSubmit(tmpPerson);
                        db.SubmitChanges();
                    }

                    PersonRoleMovieTvShowEpisodeMapping tmpMap = new PersonRoleMovieTvShowEpisodeMapping();

                    tmpMap.TvShowId = tmpItem.Id;
                    tmpMap.PersonId = tmpPerson.Id;
                    tmpMap.Role = 1;

                    db.PersonRoleMovieTvShowEpisodeMappings.InsertOnSubmit(tmpMap);
                    db.SubmitChanges();
                }

                foreach (var item in show.Creators)
                {
                    Person tmpPerson = db.Persons.SingleOrDefault(x => x.NameSurname == item.NameSurname);

                    if (tmpPerson == null)
                    {
                        tmpPerson = new Person();

                        tmpPerson.NameSurname = item.NameSurname;

                        db.Persons.InsertOnSubmit(tmpPerson);
                        db.SubmitChanges();
                    }

                    PersonRoleMovieTvShowEpisodeMapping tmpMap = new PersonRoleMovieTvShowEpisodeMapping();

                    tmpMap.TvShowId = tmpItem.Id;
                    tmpMap.PersonId = tmpPerson.Id;
                    tmpMap.Role = 2;

                    db.PersonRoleMovieTvShowEpisodeMappings.InsertOnSubmit(tmpMap);
                    db.SubmitChanges();
                }

                retId = tmpItem.Id;
            }

            return retId;
        }
    }
}
