﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tube.am.Absract.Models;
using Tube.am.DB;

namespace Tube.am.DAL.Managers
{
    public class UserManager
    {
        public static UserModel GetUserById(string id)
        {
            UserModel retItem = new UserModel();

            using (DbDataContext db = new DbDataContext())
            {
                AspNetUser tmpUser = db.AspNetUsers.SingleOrDefault(x => x.Id == id);

                if (tmpUser == null)
                    return retItem;

                retItem.Email = tmpUser.Email;
                retItem.EmailConfirmed = tmpUser.EmailConfirmed;
                retItem.Id = tmpUser.Id;
                retItem.LastLogoutDate = tmpUser.LastLogoutDate;
                retItem.NameSurname = tmpUser.NameSurename;
                retItem.PasswordHash = tmpUser.PasswordHash;
                retItem.Username = tmpUser.UserName;
                retItem.ChannelName = tmpUser.ChannelName;
                retItem.ChannelImageLink = tmpUser.ChannelImageLink;
                retItem.ChannelImageAlt = tmpUser.ChannelImageAlt;
                retItem.ChannelCoverImageLink = tmpUser.ChannelCoverImageLink;
                retItem.ChannelCoverImageAlt = tmpUser.ChannelCoverImageAlt;
                retItem.RegistrationDate = tmpUser.RegistrationDate;
            }

            return retItem;
        }

        public static List<UserModel> GetUsersSubscribedChannels(string userId)
        {
            List<UserModel> retItems = new List<UserModel>();

            using (DbDataContext db = new DbDataContext())
            {
                var query = from q in db.SubscribersMappings where q.ChannelUserId == userId select q;

                if (query == null || !query.Any())
                    return retItems;

                foreach (var item in query)
                {
                    retItems.Add(GetUserById(item.SubscriberUserId));
                }
            }

            return retItems;
        }

        public static List<UserModel> GetTop3Channels(List<UserModel> channels)
        {
            if (channels.Count <= 3)
                return channels;

            List<UserModel> retItems = new List<UserModel>();

            using (DbDataContext db = new DbDataContext())
            {
                var query = from q in db.Videos join t in channels on q.UserId equals t.Id select q;

                query = query.OrderByDescending(x => x.UploadDate);

                foreach (var item in query)
                {
                    if (!retItems.Any(x => x.Id == item.UserId))
                        retItems.Add(UserManager.GetUserById(item.UserId));

                    if (retItems.Count == 3)
                        break;
                }
            }

            return retItems;
        }

        public static int GetChannelsSubscribersCount(string channelId)
        {
            int count;

            using (DbDataContext db = new DbDataContext())
            {
                var query = from q in db.SubscribersMappings where q.SubscriberUserId == channelId select q;

                if (query == null || !query.Any())
                    return 0;

                count = query.Count();
            }

            return count;
        }

        public static List<UserModel> GetChannelsSubscribers(string channelId)
        {
            List<UserModel> retItems = new List<UserModel>();

            using (DbDataContext db = new DbDataContext())
            {
                var query = from q in db.SubscribersMappings where q.SubscriberUserId == channelId select q;

                if (query == null || !query.Any())
                    return retItems;

                foreach (var item in query)
                {
                    retItems.Add(GetUserById(item.ChannelUserId));
                }
            }

            return retItems;
        }

        public static int GetUsersSubscribedChannelNewVideosCount(DbDataContext db, string userId, string channelId)
        {
            int count;

            SubscribersMapping tmpMap = db.SubscribersMappings.SingleOrDefault(q => q.ChannelUserId == userId && q.SubscriberUserId == channelId);

            if (tmpMap == null)
                return 0;

            DateTime subscribedDate = tmpMap.Date;

            var query = from q in db.Videos where q.UserId == channelId select q;

            if (query == null || !query.Any())
                return 0;

            query = query.Where(x => x.UploadDate >= subscribedDate);

            if (query == null || !query.Any())
                return 0;

            var watchedQuery = from q in db.UserWatchedVideosMappings where q.UserId == userId select q;

            if (watchedQuery == null || !watchedQuery.Any())
                return query.Count();

            count = query.Count();

            foreach (var item in watchedQuery)
            {
                if (query.Any(x => x.Id == item.VideoId))
                    count--;
            }

            return count;
        }

        public static Dictionary<UserModel, int> GetUsersSubscribedChannelsWithNewVideoCount(string userId)
        {
            Dictionary<UserModel, int> retItems = new Dictionary<UserModel, int>();

            List<UserModel> subscrbedChannels = GetUsersSubscribedChannels(userId);

            using (DbDataContext db = new DbDataContext())
            {
                foreach (var item in subscrbedChannels)
                {
                    retItems.Add(item, GetUsersSubscribedChannelNewVideosCount(db, userId, item.Id));
                }
            }

            return retItems;
        }

        public static void Subscribe(string myId, string channelId)
        {
            using (DbDataContext db = new DbDataContext())
            {
                SubscribersMapping tmpMap = new SubscribersMapping();

                tmpMap.ChannelUserId = myId;
                tmpMap.SubscriberUserId = channelId;
                tmpMap.Date = DateTime.Now;

                db.SubscribersMappings.InsertOnSubmit(tmpMap);
                db.SubmitChanges();
            }
        }

        public static void Unsubscribe(string myId, string channelId)
        {
            using (DbDataContext db = new DbDataContext())
            {
                SubscribersMapping tmpMap = db.SubscribersMappings.SingleOrDefault(x => x.ChannelUserId == myId && x.SubscriberUserId == channelId);

                if (tmpMap == null)
                    return;

                db.SubscribersMappings.DeleteOnSubmit(tmpMap);
                db.SubmitChanges();
            }
        }

        public static void AddVideoToHistory(string userId, int videoId)
        {
            using (DbDataContext db = new DbDataContext())
            {
                UserWatchedVideosMapping map = db.UserWatchedVideosMappings.SingleOrDefault(x => x.UserId == userId && x.VideoId == videoId);

                if (map == null)
                {
                    map = new UserWatchedVideosMapping();

                    map.UserId = userId;
                    map.VideoId = videoId;
                    map.Date = DateTime.Now;

                    db.UserWatchedVideosMappings.InsertOnSubmit(map);
                    db.SubmitChanges();
                }
                else
                {
                    map.Date = DateTime.Now;

                    db.SubmitChanges();
                }
            }
        }
    }
}
