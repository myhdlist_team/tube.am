﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Tube.am.Absract.Models;
using Tube.am.DB;

namespace Tube.am.DAL.Managers
{
    public class VideoManager
    {
        public static VideoModel GetVideoById(DbDataContext db, int id)
        {
            VideoModel retItem = new VideoModel();

            Video tmpVideo = db.Videos.SingleOrDefault(x => x.Id == id);

            retItem.Id = tmpVideo.Id;
            retItem.Link = tmpVideo.Link;
            retItem.Name = tmpVideo.Name;
            retItem.Description = tmpVideo.Description;
            retItem.Duration = TimeSpan.FromSeconds(tmpVideo.Duration);
            retItem.UploadedDate = tmpVideo.UploadDate;
            retItem.Is360 = tmpVideo.Is360;
            retItem.Is480 = tmpVideo.Is480;
            retItem.Is720 = tmpVideo.Is720;
            retItem.ViewCount = tmpVideo.ViewCount;
            retItem.Type = tmpVideo.Type;
            retItem.ThumbnailImageLink = tmpVideo.ThumbnailImageLink;
            retItem.ThumbnailImageAlt = tmpVideo.ThumbnailImageAlt;

            var query = from q in db.Votes where q.VideoId == id select q;

            if (query == null || !query.Any())
            {
                retItem.LikesCount = 0;
                retItem.DislikesCount = 0;
            }

            retItem.LikesCount = query.Count(x => x.IsLiked);
            retItem.DislikesCount = query.Count(x => !x.IsLiked);
            retItem.SearchTerm = tmpVideo.SearchTerm;
            retItem.Channel = UserManager.GetUserById(tmpVideo.UserId);
            retItem.Comments = CommentManager.GetCommentsByVideo(db, tmpVideo.Id);
            retItem.Screenshots = ScreenshotManager.GetScreenshotsByVideoId(id);

            return retItem;
        }

        public static VideoModel GetVideoById(int id)
        {
            return GetVideoById(new DbDataContext(), id);
        }

        public static List<VideoModel> GetTop3Videos()
        {
            List<VideoModel> retItems = new List<VideoModel>();

            using (DbDataContext db = new DbDataContext())
            {
                var query = from q in db.Videos where q.IsActive select q;

                if (query == null || !query.Any())
                    return retItems;

                query = query.OrderByDescending(x => x.ViewCount).Take(3);

                foreach (var item in query)
                {
                    retItems.Add(GetVideoById(db, item.Id));
                }
            }

            return retItems;
        }

        public static List<VideoModel> GetMostViewedVideos()
        {
            List<VideoModel> retItems = new List<VideoModel>();

            using (DbDataContext db = new DbDataContext())
            {
                var query = from q in db.Videos where q.IsActive select q;

                if (query == null || !query.Any())
                    return retItems;

                query = query.OrderByDescending(x => x.ViewCount).Skip(3).Take(8); //3 and 8 are fixed values

                foreach (var item in query)
                {
                    retItems.Add(GetVideoById(db, item.Id));
                }
            }

            return retItems;
        }

        public static List<VideoModel> GetUserAllVideos(string userId)
        {
            List<VideoModel> retItems = new List<VideoModel>();

            using (DbDataContext db = new DbDataContext())
            {
                var query = from q in db.Videos where q.IsActive && q.UserId == userId select q;

                if (query == null || !query.Any())
                    return retItems;

                query = query.OrderByDescending(x => x.UploadDate);

                foreach (var item in query)
                {
                    retItems.Add(GetVideoById(db, item.Id));
                }
            }

            return retItems;
        }

        public static List<VideoModel> GetPlaylistVideos(DbDataContext db, int playlistId)
        {
            List<VideoModel> retItems = new List<VideoModel>();

            var query = from q in db.PlaylistVideoMappings where q.PlaylistId == playlistId select q;

            if (query == null || !query.Any())
                return retItems;

            foreach (var item in query)
            {
                if (item.Video.IsActive)
                    retItems.Add(GetVideoById(db, item.VideoId));
            }

            return retItems;
        }

        public static List<VideoModel> GetRelatedVideos(int videoId, string userId)
        {
            List<VideoModel> retItems = new List<VideoModel>();

            //TODO: related Videos logic

            return retItems;
        }


        public static List<VideoModel> GetUsersLikedVideos(string userId)
        {
            List<VideoModel> retItems = new List<VideoModel>();

            using (DbDataContext db = new DbDataContext())
            {
                var query = from q in db.Votes where q.UserId == userId && q.IsLiked select q;

                if (query == null || !query.Any())
                    return retItems;

                foreach (var item in query)
                {
                    if (item.Video.IsActive)
                        retItems.Add(GetVideoById(db, item.VideoId));
                }
            }

            return retItems;
        }

        public static List<VideoModel> GetUsersWatchLaterVideos(string userId)
        {
            List<VideoModel> retItems = new List<VideoModel>();

            using (DbDataContext db = new DbDataContext())
            {
                var query = from q in db.WatchLaterUserVideoMappings where q.UserId == userId select q;

                if (query == null || !query.Any())
                    return retItems;

                foreach (var item in query)
                {
                    if (item.Video.IsActive)
                        retItems.Add(GetVideoById(db, item.VideoId));
                }
            }

            return retItems;
        }

        public static List<VideoModel> GetUsersHistoryVideos(string userId)
        {
            List<VideoModel> retItems = new List<VideoModel>();

            using (DbDataContext db = new DbDataContext())
            {
                var query = from q in db.UserWatchedVideosMappings where q.UserId == userId select q;

                if (query == null || !query.Any())
                    return retItems;

                foreach (var item in query)
                {
                    if (item.Video.IsActive)
                        retItems.Add(GetVideoById(db, item.VideoId));
                }
            }

            return retItems;
        }

        public static List<string> GetSuggestedThumbnailsLinks(string videoName)
        {
            List<string> retItems = new List<string>();

            string screenshotsPath = videoName;

            List<string> tmpItems = Directory.GetFiles(screenshotsPath).ToList();

            foreach (var item in tmpItems)
            {
                retItems.Add(item.Substring(item.IndexOf("\\Content\\Screenshots")));
            }

            return retItems.Take(12).ToList();
        }

        public static void AddVideoToPlaylists(int videoId, int playlistId)
        {
            using (DbDataContext db = new DbDataContext())
            {
                PlaylistVideoMapping map = db.PlaylistVideoMappings.SingleOrDefault(x => x.VideoId == videoId && x.PlaylistId == playlistId);

                if (map == null)
                {
                    map = new PlaylistVideoMapping();

                    map.PlaylistId = playlistId;
                    map.VideoId = videoId;

                    db.PlaylistVideoMappings.InsertOnSubmit(map);
                    db.SubmitChanges();
                }
                else
                {
                    db.PlaylistVideoMappings.DeleteOnSubmit(map);
                    db.SubmitChanges();
                }
            }

            return;
        }

        /// <summary>
        /// Like or Dislike video by user
        /// </summary>
        /// <param name="videoId">Video id which is being liked or disliked</param>
        /// <param name="userId">User id who is liking or disliking the video</param>
        /// <param name="isLike">True to like , and false to dislike the video</param>
        public static void LikeDislikeVideo(int videoId, string userId, bool isLike)
        {
            using (DbDataContext db = new DbDataContext())
            {
                Vote tmpVote = db.Votes.SingleOrDefault(x => x.VideoId == videoId && x.UserId == userId);

                if (tmpVote != null)
                {
                    if (tmpVote.IsLiked != isLike)
                    {
                        tmpVote.IsLiked = isLike;
                        tmpVote.Date = DateTime.Now;

                        db.SubmitChanges();
                    }
                    else
                    {
                        db.Votes.DeleteOnSubmit(tmpVote);

                        db.SubmitChanges();
                    }
                }
                else
                {
                    tmpVote = new Vote();

                    tmpVote.VideoId = videoId;
                    tmpVote.UserId = userId;
                    tmpVote.IsLiked = isLike;
                    tmpVote.Date = DateTime.Now;

                    db.Votes.InsertOnSubmit(tmpVote);
                    db.SubmitChanges();
                }
            }
        }

        public static void AddVideoToWatchLater(int videoId, string userId)
        {
            using (DbDataContext db = new DbDataContext())
            {
                WatchLaterUserVideoMapping tmpMap = db.WatchLaterUserVideoMappings.SingleOrDefault(x => x.VideoId == videoId && x.UserId == userId);

                if (tmpMap == null)
                {
                    tmpMap = new WatchLaterUserVideoMapping();

                    tmpMap.VideoId = videoId;
                    tmpMap.UserId = userId;
                    tmpMap.Date = DateTime.Now;

                    db.WatchLaterUserVideoMappings.InsertOnSubmit(tmpMap);
                    db.SubmitChanges();
                }
                else
                {
                    db.WatchLaterUserVideoMappings.DeleteOnSubmit(tmpMap);
                    db.SubmitChanges();
                }
            }
        }

        public static int AllocateVideoSpace(string userId)
        {
            int retId = 0;

            using (DbDataContext db = new DbDataContext())
            {
                Video tmpVideo = new Video();

                tmpVideo.Name = "temp";
                tmpVideo.Link = "temp";
                tmpVideo.Duration = 0;
                tmpVideo.UploadDate = DateTime.Now;
                tmpVideo.ViewCount = 0;
                tmpVideo.ThumbnailImageAlt = "temp";
                tmpVideo.ThumbnailImageLink = "temp";
                tmpVideo.UserId = userId;
                tmpVideo.Is360 = false;
                tmpVideo.Is480 = false;
                tmpVideo.Is720 = false;
                tmpVideo.Type = null;
                tmpVideo.IsActive = false;

                db.Videos.InsertOnSubmit(tmpVideo);
                db.SubmitChanges();

                retId = tmpVideo.Id;
            }

            return retId;
        }

        public static void SetThumbnailToVideo(int videoId, string link)
        {
            using (DbDataContext db = new DbDataContext())
            {
                Video tmpVideo = db.Videos.SingleOrDefault(x => x.Id == videoId);

                if (tmpVideo == null)
                    return;

                tmpVideo.ThumbnailImageLink = link;
                tmpVideo.ThumbnailImageAlt = "temp";
                tmpVideo.IsActive = false;

                db.SubmitChanges();
            }
        }

        public static int FillVideoData(int videoId, VideoModel video)
        {
            StringBuilder searchTerm = new StringBuilder("");
            int retId;

            using (DbDataContext db = new DbDataContext())
            {
                Video tmpItem = db.Videos.SingleOrDefault(x => x.Id == videoId);

                if (tmpItem == null)
                    return 0;

                tmpItem.Description = video.Description;
                tmpItem.Duration = (int)video.Duration.TotalSeconds;
                tmpItem.Link = video.Link;
                tmpItem.Name = video.Name;
                tmpItem.ViewCount = 0;
                tmpItem.Is360 = video.Is360;
                tmpItem.Is480 = video.Is480;
                tmpItem.Is720 = video.Is720;
                tmpItem.IsActive = true;

                searchTerm.Append(video.Name).Append(" ").Append(video.Description);//TODO: more fields to concat? 
                tmpItem.SearchTerm = searchTerm.ToString();

                db.SubmitChanges();

                retId = tmpItem.Id;
            }

            return retId;
        }

        public static int AddVideoItem(VideoModel video)
        {
            StringBuilder searchTerm = new StringBuilder("");
            int retId;

            using (DbDataContext db = new DbDataContext())
            {
                Video tmpItem = new Video();

                tmpItem.Description = video.Description;
                tmpItem.Duration = video.Duration.Seconds;
                tmpItem.Link = video.Link;
                tmpItem.Name = video.Name;
                tmpItem.ViewCount = video.ViewCount; // TODO: Set 0 or set greabbed viewcount
                tmpItem.UserId = ""; // TODO: set deafult user id
                tmpItem.Is360 = video.Is360;
                tmpItem.Is480 = video.Is480;
                tmpItem.Is720 = video.Is720;

                searchTerm.Append(video.Name).Append(" ").Append(video.Description);//TODO: more fields to concat? 
                tmpItem.SearchTerm = searchTerm.ToString();

                db.Videos.InsertOnSubmit(tmpItem);
                db.SubmitChanges();

                //TODO: Add video screenshots to folder or may be grabber will do it for me
                //foreach(var item in video.Screenshots)
                //{
                //    Screenshot tmpScreen = new Screenshot();

                //    tmpScreen.Alt = item.Alt;
                //    tmpScreen.Link = item.Link;
                //    tmpScreen.VideoId = tmpItem.Id;

                //    db.Screenshots.InsertOnSubmit(tmpScreen);
                //    db.SubmitChanges();
                //}

                retId = tmpItem.Id;
            }

            return retId;
        }
    }
}
