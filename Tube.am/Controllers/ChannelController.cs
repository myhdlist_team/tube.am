﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tube.am.Absract.Models;
using Tube.am.DAL.Managers;
using Tube.am.ViewModels;

namespace Tube.am.Controllers
{
    public class ChannelController : Controller
    {
        // GET: Channel
        public ActionResult Index(string id)
        {
            if (String.IsNullOrEmpty(id))
                return Redirect("/Home/Index");

            UserModel loggedInUser = UserModelView.LoggedInUser;

            UserModel channel = UserManager.GetUserById(id);

            ChannelViewModel model = new ChannelViewModel();

            if (loggedInUser.Id != null)
            {
                model.LoggedIn = true;

                if (loggedInUser.Id == channel.Id)
                    model.MyPage = true;
                else
                {
                    model.IsSubscribed = false;

                    if (UserManager.GetUsersSubscribedChannels(loggedInUser.Id).Any(x => x.Id == channel.Id))
                        model.IsSubscribed = true;
                }
            }

            if (model.MyPage)
                model.Page = "mychannel";
            model.Channel = channel;
            model.SubscribersCount = UserManager.GetChannelsSubscribersCount(channel.Id);
            model.Videos = VideoManager.GetUserAllVideos(channel.Id);
            model.Movies = MovieManager.GetUsersAddedMovies(channel.Id);
            model.Playlists = PlaylistManager.GetUserPlaylists(channel.Id);

            return View(model);
        }

        public ActionResult LikedVideos()
        {
            if (UserModelView.LoggedInUser.Id == null)
                return Redirect("/Home/Index");

            LikedHistoryWatchLaterVideosViewModel model = new LikedHistoryWatchLaterVideosViewModel();
            model.Page = "liked";
            model.Videos = VideoManager.GetUsersLikedVideos(UserModelView.LoggedInUser.Id);

            return View(model);
        }

        public ActionResult HistoryVideos()
        {
            if (UserModelView.LoggedInUser.Id == null)
                return Redirect("/Home/Index");

            LikedHistoryWatchLaterVideosViewModel model = new LikedHistoryWatchLaterVideosViewModel();
            model.Page = "history";
            model.Videos = VideoManager.GetUsersHistoryVideos(UserModelView.LoggedInUser.Id);

            return View(model);
        }

        public ActionResult WatchLaterVideos()
        {
            if (UserModelView.LoggedInUser.Id == null)
                return Redirect("/Home/Index");

            LikedHistoryWatchLaterVideosViewModel model = new LikedHistoryWatchLaterVideosViewModel();
            model.Page = "watchlater";
            model.Videos = VideoManager.GetUsersWatchLaterVideos(UserModelView.LoggedInUser.Id);

            return View(model);
        }

        [HttpPost]
        public ActionResult Subscribe(string myId, string channelId)
        {
            if (String.IsNullOrEmpty(myId) || String.IsNullOrEmpty(channelId))
                return Json("fail", JsonRequestBehavior.AllowGet);

            UserManager.Subscribe(myId, channelId);

            return Json("Success", JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult Unsubscribe(string myId, string channelId)
        {
            if (String.IsNullOrEmpty(myId) || String.IsNullOrEmpty(channelId))
                return Json("fail", JsonRequestBehavior.AllowGet);

            UserManager.Unsubscribe(myId, channelId);

            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public ActionResult UploadVideo()
        {
            GlobalViewModel model = new GlobalViewModel();
            model.Page = "";
            return View(model);
        }

        public ActionResult UploadInfoPartial()
        {
            return PartialView("UploadVideoInfoPartialView");
        }

        [HttpPost]
        public JsonResult Upload()
        {
            string fileName = Request.Headers["X-File-Name"];
            string fileType = Request.Headers["X-File-Type"];
            int fileSize = Convert.ToInt32(Request.Headers["X-File-Size"]);

            System.IO.Stream fileContent = Request.InputStream;
            //Creating a FileStream to save file's content
            System.IO.FileStream fileStream = System.IO.File.Create(Server.MapPath("~/Content/Originals/") + fileName);
            fileContent.Seek(0, System.IO.SeekOrigin.Begin);
            //Copying file's content to FileStream
            fileContent.CopyTo(fileStream);
            fileStream.Dispose();

            return Json("finished");
        }

        public ActionResult ChannelVideos(string channelId)
        {
            return PartialView("ChannelVideosPartialView", new ChannelVideosPartialViewModel() { Channel = UserManager.GetUserById(channelId), Videos = VideoManager.GetUserAllVideos(channelId) });
        }

        public ActionResult ChannelPlaylists(string channelId)
        {
            return PartialView("ChannelPlaylistsPartialView", new ChannelPlaylistsPartialViewModel() { Channel = UserManager.GetUserById(channelId), Playlists = PlaylistManager.GetUserPlaylists(channelId) });
        }

        public ActionResult ChannelSettings(string channelId)
        {
            return PartialView("ChannelSettingsPartialView", new ChannelSettingsPartialViewModel() { Channel = UserManager.GetUserById(channelId) });
        }

        public ActionResult SuggestVideoThumbnails(string videoName, int videoId)
        {
            ThumbnailsViewModel model = new ThumbnailsViewModel();

            model.ThumbnailLinks = VideoManager.GetSuggestedThumbnailsLinks(videoName);
            model.VideoId = videoId;

            return PartialView("UploadVideoInfoThumbnailsPartialView", model);
        }

        [HttpPost]
        public ActionResult SetThumbnail(int videoId, string link)
        {
            VideoManager.SetThumbnailToVideo(videoId, link);

            return Json(new { message = "success" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveVideo(int id, string name, string description, string link, int duration)
        {
            VideoModel tmp = new VideoModel();
            tmp.Id = id;
            tmp.Description = description;
            tmp.Duration = TimeSpan.FromSeconds(duration);
            tmp.Link = link;
            tmp.Name = name;
            tmp.ThumbnailImageAlt = name;

            VideoManager.FillVideoData(id, tmp);

            //return Redirect("/Channel/Index?id=" + UserModelView.LoggedInUser.Id != null ? UserModelView.LoggedInUser.Id : "");
            return Redirect("/Home/Video?id=" + id);
        }
    }
}