﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tube.am.Absract.Models;
using Tube.am.DAL.Managers;
using Tube.am.ViewModels;

namespace Tube.am.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            IndexViewModel model = new IndexViewModel();

            model.Page = "home";

            return View(model);
        }

        public ActionResult Video(int id, int playlistId = 0)
        {
            VideoViewModel model = new VideoViewModel();

            UserModel loggedInUser = UserModelView.LoggedInUser; ;

            model.Video = VideoManager.GetVideoById(id);

            UserModel channel = UserManager.GetUserById(model.Video.Channel.Id);

            if (loggedInUser.Id != null)
            {
                model.LoggedIn = true;

                if (loggedInUser.Id == channel.Id)
                    model.MyPage = true;
                else
                {
                    model.IsSubscribed = false;

                    if (UserManager.GetUsersSubscribedChannels(loggedInUser.Id).Any(x => x.Id == channel.Id))
                        model.IsSubscribed = true;
                }

                UserManager.AddVideoToHistory(loggedInUser.Id, id);
            }

            model.RelatedVideos = VideoManager.GetRelatedVideos(id, loggedInUser.Id);

            if (playlistId != 0)
            {
                model.IsPlaylist = true;
                model.Playlist = PlaylistManager.GetPlaylistById(playlistId);
            }
            else
            {
                model.IsPlaylist = false;
            }

            return View(model);
        }

        public ActionResult LikeDislikeVideo(bool type, int videoId, string userId)
        {
            if (videoId <= 0 || String.IsNullOrEmpty(userId))
                return Redirect("/Home/Video?id=" + videoId);

            VideoManager.LikeDislikeVideo(videoId, userId, type);

            return PartialView("LikeDislikeSectionPartialView", new LikeDislikePartialViewModel() { Video = VideoManager.GetVideoById(videoId) });
        }

        public ActionResult AddToPlaylist(int playlistId, int videoId)
        {
            if (videoId <= 0 || playlistId <= 0)
                return Redirect("/Home/Video?id=" + videoId);

            VideoManager.AddVideoToPlaylists(videoId, playlistId);

            return PartialView("AddToPlaylistPartialView", new AddToPlaylistPartialViewModel() { Video = VideoManager.GetVideoById(videoId) });
        }

        public ActionResult AddToWatchLater(int videoId, string userId)
        {
            if (videoId <= 0 || String.IsNullOrEmpty(userId))
                return Redirect("/Home/Video?id=" + videoId);

            VideoManager.AddVideoToWatchLater(videoId, userId);

            return PartialView("AddToPlaylistPartialView", new AddToPlaylistPartialViewModel() { Video = VideoManager.GetVideoById(videoId) });
        }

        public ActionResult AddNewPlaylist(string playlistName, string userId, int videoId)
        {
            if (String.IsNullOrEmpty(playlistName) || String.IsNullOrEmpty(userId))
                return Redirect("/Home/Video?id=" + videoId);

            PlaylistManager.AddPlaylistForUser(userId, playlistName, videoId);

            return PartialView("AddToPlaylistPartialView", new AddToPlaylistPartialViewModel() { Video = VideoManager.GetVideoById(videoId), LoggedInUser = UserModelView.LoggedInUser });
        }
    }
}