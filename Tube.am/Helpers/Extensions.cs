﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Tube.am
{
    public static class Extensions
    {
        const int SECOND = 1;
        const int MINUTE = 60 * SECOND;
        const int HOUR = 60 * MINUTE;
        const int DAY = 24 * HOUR;
        const int MONTH = 30 * DAY;

        public static bool IsNullOrEmpty<T>(this IEnumerable<T> enumerable)
        {
            if (enumerable == null)
            {
                return true;
            }
            /* If this is a list, use the Count property for efficiency. 
             * The Count property is O(1) while IEnumerable.Count() is O(N). */
            var collection = enumerable as ICollection<T>;
            if (collection != null)
            {
                return collection.Count < 1;
            }
            return !enumerable.Any();
        }

        public static string GetRelativeTime(this DateTime timeToCompare)
        {
            var ts = new TimeSpan(DateTime.Now.Ticks - timeToCompare.Ticks);
            double delta = Math.Abs(ts.TotalSeconds);

            if (delta < 1 * MINUTE)
            {
                return ts.Seconds == 1 ? "մեկ վարկյան առաջ" : ts.Seconds + " վարկյան առաջ";
            }
            if (delta < 2 * MINUTE)
            {
                return "մեկ րոպե առաջ";
            }
            if (delta < 45 * MINUTE)
            {
                return ts.Minutes + " րոպե առաջ";
            }
            if (delta < 90 * MINUTE)
            {
                return "մեկ ժամ առաջ";
            }
            if (delta < 24 * HOUR)
            {
                return ts.Hours + " ժամ առաջ";
            }
            if (delta < 48 * HOUR)
            {
                return "երեկ";
            }
            if (delta < 30 * DAY)
            {
                return ts.Days + " օր առաջ";
            }
            if (delta < 12 * MONTH)
            {
                int months = Convert.ToInt32(Math.Floor((double)ts.Days / 30));
                return months <= 1 ? "մեկ ամիս առաջ" : months + " ամիս առաջ";
            }
            else
            {
                int years = Convert.ToInt32(Math.Floor((double)ts.Days / 365));
                return years <= 1 ? "մեկ տարի առաջ" : years + " տարի առաջ";
            }
        }
    }
}