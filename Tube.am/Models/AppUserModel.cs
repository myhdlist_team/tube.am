﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace Tube.am.Models
{
    public class AppUser : IdentityUser
    {
        public string NameSurename { get; set; }

        public string ChannelName { get; set; }

        public DateTime RegistrationDate { get; set; }

        public DateTime? LastLogoutDate { get; set; }

        public string ChannelImageLink { get; set; }

        public string ChannelImageAlt { get; set; }

        public string ChannelCoverImageLink { get; set; }

        public string ChannelCoverImageAlt { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<AppUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }
}