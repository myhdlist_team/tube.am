﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Timers;
using System.Web;
using Tube.am.DAL.Managers;

namespace Tube.am
{
    public class ProcessHub : Hub
    {
        public string msg = "Uploading original";
        string strCmdText;
        string ffmpegPath = @"C:\Program Files\ffmpeg\bin\ffmpeg.exe";
        string ffprobePath = @"C:\Program Files\ffmpeg\bin\ffprobe.exe";
        string path = @"D:\Projects\Tube.am\Tube.am\Content\Originals\";
        string videosPath = @"D:\Projects\Tube.am\Tube.am\Content\Videos\";
        string screenshotsPath = @"D:\Projects\Tube.am\Tube.am\Content\Screenshots\";
        string logsPath = @"D:\Projects\Tube.am\Tube.am\Content\Logs\";
        double progress = 0.0;
        Timer t;

        public ProcessHub()
        {
            t = new Timer();
            t.Interval = 10;
            t.Elapsed += T_Elapsed;
        }

        private void T_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (progress < 12)
                progress += 0.17;
            if (progress > 12)
                progress += 0.1;
            if (progress > 14)
                progress += 0.1;
            if (progress >= 15)
            {
                progress = 15;
                return;
            }
            UploadOriginal(progress.ToString());
        }

        public void UploadOriginal(string response, string userId = null)
        {
            if (response == "start")
            {
                t.Start();

                int id = VideoManager.AllocateVideoSpace(userId);

                Clients.Caller.sendMessage("allocated " + id);
            }
            if (response != "finished")
            {
                Clients.Caller.sendMessage(string.Format(msg + " {0}", (int)Math.Floor(progress)));
            }
            else
            {
                t.Stop();
                t.Dispose();
                progress = 15;
                Clients.Caller.sendMessage(string.Format(msg + " {0}", (int)Math.Floor(progress)));
            }
        }

        public void SaveVideo(string fileName, string videoId)
        {
            int videoDuration = 0;
            string videoLink = videosPath.Substring(videosPath.IndexOf("\\Content\\Videos\\")) + videoId + ".mp4"; 

            t.Stop();
            //fileName = fileName.Substring(1, fileName.Length - 2);

            strCmdText = "-v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 "
                + path + fileName;

            System.Diagnostics.Process process = new System.Diagnostics.Process();

            process.StartInfo.Arguments = strCmdText;
            process.StartInfo.FileName = ffprobePath;

            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;
            process.StartInfo.CreateNoWindow = false;

            if (!process.Start())
            {
                return;
            }

            StreamReader reader = process.StandardOutput;
            StringBuilder sb = new StringBuilder();

            string line;
            while ((line = reader.ReadLine()) != null)
            {
                int tmp;
                Int32.TryParse(line.Split('.')[0], out tmp);
                if (tmp != 0)
                    videoDuration = tmp;
            }

            process.Close();

            msg = "Saving screenshots...";

            Directory.CreateDirectory(screenshotsPath + videoId + "\\");

            strCmdText = "-i "
                + path + fileName +
                " -qscale:v 1 -vf fps=1/6 -f image2 "
                + screenshotsPath + videoId + "\\" + "img%03d.jpg\"";

            process = new System.Diagnostics.Process();

            process.StartInfo.Arguments = strCmdText;
            process.StartInfo.FileName = ffmpegPath;

            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;
            process.StartInfo.CreateNoWindow = false;

            if (!process.Start())
            {
                return;
            }

            reader = process.StandardError;
            sb = new StringBuilder();

            line = "";
            while ((line = reader.ReadLine()) != null)
            {
                sb.AppendLine(line);
                if (progress > 23)
                {
                    if (progress < 24.8)
                        progress += 0.1;
                    else
                        progress = 24.8;
                }
                else
                    progress += 0.3;

                Clients.Caller.sendMessage(string.Format(msg + " {0}", (int)Math.Floor(progress)));
            }
            progress = 25;
            process.Close();

            msg = "Screenshots Saved";

            Clients.Caller.sendMessage(string.Format(msg + " {0}", (int)Math.Floor(progress)));
            Clients.Caller.sendMessage("getScreenshots " + screenshotsPath + videoId + "\\");

            msg = "Saving Logs...";

            strCmdText = "-i "
                + path + fileName
                + " -c:v libx264 -b:v 1200k -c:a libvo_aacenc -b:a 96k"
                + " -vf scale=trunc(oh*a/2)*2:480 -ar 48000 -ac 2 -preset medium"
                + " -flags +loop -cmp chroma -crf 21 -refs 1 -coder 0 -me_method full"
                + " -me_range 16 -subq 5 -partitions +parti4x4+parti8x8+partp8x8 -g 250"
                + " -keyint_min 25 -level 30 -trellis 2 -sc_threshold 40 -i_qfactor 0.71 -f null -an -passlogfile "
                + logsPath + videoId + "test.log"
                + " -pass 1 -y "
                + videosPath + videoId + ".mp4\"";

            process = new System.Diagnostics.Process();

            process.StartInfo.Arguments = strCmdText;
            process.StartInfo.FileName = ffmpegPath;

            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;
            process.StartInfo.CreateNoWindow = false;

            if (!process.Start())
            {
                return;
            }

            reader = process.StandardError;
            sb = new StringBuilder();

            line = "";
            while ((line = reader.ReadLine()) != null)
            {
                sb.AppendLine(line);
                if (line.StartsWith("frame="))
                {
                    if (progress < 31)
                    {
                        progress += 0.8;
                    }
                    else
                    {
                        if (progress < 44)
                            progress += 0.4;
                        else if (progress < 49)
                            progress += 0.15;
                        else if (progress < 49.5)
                            progress += 0.05;
                        else progress = 49.9;
                    }
                }

                Clients.Caller.sendMessage(string.Format(msg + " {0}", (int)Math.Floor(progress)));
            }
            progress = 50;
            process.Close();

            msg = "Logs Saved";

            Clients.Caller.sendMessage(string.Format(msg + " {0}", (int)Math.Floor(progress)));

            msg = "Converting Video...";

            strCmdText = "-y -i "
                + path + fileName
                + " -c:v libx264 -b:v 1200k -c:a libvo_aacenc -b:a 96k"
                + " -vf scale=trunc(oh*a/2)*2:480 -ar 48000 -ac 2 -preset medium"
                + " -flags +loop -cmp chroma -crf 21 -refs 1 -coder 0 -me_method full"
                + " -me_range 16 -subq 5 -partitions +parti4x4+parti8x8+partp8x8 -g 250"
                + " -keyint_min 25 -level 30 -trellis 2 -sc_threshold 40 -i_qfactor 0.71 -an -passlogfile "
                + logsPath + videoId + "test.log"
                + " -pass 2 "
                + videosPath + videoId + ".mp4\"";

            process = new System.Diagnostics.Process();

            process.StartInfo.Arguments = strCmdText;
            process.StartInfo.FileName = ffmpegPath;

            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;
            process.StartInfo.CreateNoWindow = false;

            if (!process.Start())
            {
                return;
            }

            reader = process.StandardError;
            sb = new StringBuilder();

            line = "";
            while ((line = reader.ReadLine()) != null)
            {
                sb.AppendLine(line);
                if (line.StartsWith("frame="))
                {
                    if (progress < 65)
                    {
                        progress += 0.8;
                    }
                    else
                    {
                        if (progress < 82)
                            progress += 0.4;
                        else if (progress < 95)
                            progress += 0.15;
                        else if (progress < 99.5)
                            progress += 0.05;
                        else
                            progress = 99.9;
                    }
                }

                Clients.Caller.sendMessage(string.Format(msg + " {0}", (int)Math.Floor(progress)));
            }
            progress = 100;
            process.Close();

            msg = "Saving Completed";

            Clients.Caller.sendMessage(string.Format(msg + " {0} {1} {2}", (int)Math.Floor(progress), videoLink, videoDuration));
        }
    }
}