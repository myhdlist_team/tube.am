

function subscribe(myId, channelId) {
    $.ajax({
        type: "POST",
        url: "/Channel/Subscribe?myId=" + myId + "&channelId=" + channelId,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            location.reload();
        }
    });
}

function unsubscribe(myId, channelId) {
    $.ajax({
        type: "POST",
        url: "/Channel/Unsubscribe?myId=" + myId + "&channelId=" + channelId,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            location.reload();
        }
    });
}

var likeDislikeVideo = function (type, videoId, userId) {
    $('#player .views').load('/Home/LikeDislikeVideo?type=' + type + '&videoId=' + videoId + '&userId=' + userId);
}

var addToWatchLater = function (videoId, userId) {
    $('#player .lists').load('/Home/AddToWatchLater?videoId=' + videoId + '&userId=' + userId);
}

var addToPlaylist = function (playlistId, videoId) {
    $('#player .lists').load('/Home/AddToPlaylist?playlistId=' + playlistId + '&videoId=' + videoId);
}

var addNewPlaylist = function (playlistName, userId, videoId) {
    if (playlistName.length > 0 && playlistName.trim()) {
        $('#player .lists').load('/Home/AddNewPlaylist?playlistName=' + playlistName + '&userId=' + userId + '&videoId=' + videoId);
    }
    else {
        //TODO: show may be a popup , or change the input style
    }
}

var redirectToVideos = function (that, channelId) {
    that.parent().parent().find('a').removeClass("active");
    that.addClass("active");
    $(".channel-content-wrapper").load('/Channel/ChannelVideos?channelId=' + channelId);
}

var redirectToPlaylists = function (that, channelId) {
    console.log(that);
    console.log(channelId);
    that.parent().parent().find('a').removeClass("active");
    that.addClass("active");
    $(".channel-content-wrapper").load('/Channel/ChannelPlaylists?channelId=' + channelId);
}

var redirectToSettings = function (that, channelId) {
    that.parent().parent().find('a').removeClass("active");
    that.addClass("active");
    $(".channel-content-wrapper").load('/Channel/ChannelSettings?channelId=' + channelId);
}

$(document).ready(function () {

    var search_show = $('header .search_btn');
    var search_close = $('#search .hide_btn');
    var search_bar = $('#search');

    function search_display(options) {
        if (options == "show") {
            search_bar.css('top', 0);
            _backdrop(options);
        }
        if (options == "hide") {
            search_bar.attr('style', -search_bar.height());
            _backdrop(options);
        }
    }

    search_show.click(function () {
        search_display('show');
    })
    search_close.click(function () {
        search_display('hide');
    })

    var burger = $('header .burger');
    var menu = $('header .menu');
    var body = $('body');

    function burger_display(options) {
        if (burger.is('.active') || options == "hide") {
            menu.css('right', '-100%');
            burger.removeClass('active');
            body.removeClass('no_scroll');
            _backdrop('hide');
        }
        else {
            menu.css('right', '0');
            burger.addClass('active');
            body.addClass('no_scroll');
            _backdrop('show');
        }
    }

    burger.click(function () {
        burger_display();
    });

    function _backdrop(options) {
        var backdrop = $('.backdrop');

        if (options == "show") {
            backdrop.stop().fadeIn(300);
        };
        if (options == "hide") {
            backdrop.stop().fadeOut(300);
        };

        backdrop.on("mousedown", function () {
            burger_display('hide');
            search_display('hide');
        })
    }




    var descr_show_more = $(".description .expand");
    var descr_opened = false;

    descr_show_more.click(function () {
        var _this = $(this);
        var descr = _this.parent().find('p');

        if (descr_opened) {
            descr.css('max-height', 100);
            _this.text('Show more');
            descr_opened = false;
        }
        else {
            descr.css('max-height', 1000);
            _this.text('Show less');
            descr_opened = true;
        }
    })



    'use strict';

    ; (function (document, window, index) {
        // feature detection for drag&drop upload
        var isAdvancedUpload = function () {
            var div = document.createElement('div');
            return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window && 'FileReader' in window;
        }();


        // applying the effect for every form
        var forms = document.querySelectorAll('.box');
        Array.prototype.forEach.call(forms, function (form) {
            var input = form.querySelector('input[type="file"]'),
				label = form.querySelector('label'),
				errorMsg = form.querySelector('.box__error span'),
				restart = form.querySelectorAll('.box__restart'),
				droppedFiles = false,
				showFiles = function (files) {
				    label.textContent = files.length > 1 ? (input.getAttribute('data-multiple-caption') || '').replace('{count}', files.length) : files[0].name;
				},
				triggerFormSubmit = function () {
				    var event = document.createEvent('HTMLEvents');
				    event.initEvent('submit', true, false);
				    form.dispatchEvent(event);
				};

            // letting the server side to know we are going to make an Ajax request
            var ajaxFlag = document.createElement('input');
            ajaxFlag.setAttribute('type', 'hidden');
            ajaxFlag.setAttribute('name', 'ajax');
            ajaxFlag.setAttribute('value', 1);
            form.appendChild(ajaxFlag);

            // automatically submit the form on file select
            input.addEventListener('change', function (e) {
                showFiles(e.target.files);


                triggerFormSubmit();


            });

            // drag&drop files if the feature is available
            if (isAdvancedUpload) {
                form.classList.add('has-advanced-upload'); // letting the CSS part to know drag&drop is supported by the browser

                ['drag', 'dragstart', 'dragend', 'dragover', 'dragenter', 'dragleave', 'drop'].forEach(function (event) {
                    form.addEventListener(event, function (e) {
                        // preventing the unwanted behaviours
                        e.preventDefault();
                        e.stopPropagation();
                    });
                });
                ['dragover', 'dragenter'].forEach(function (event) {
                    form.addEventListener(event, function () {
                        form.classList.add('is-dragover');
                    });
                });
                ['dragleave', 'dragend', 'drop'].forEach(function (event) {
                    form.addEventListener(event, function () {
                        form.classList.remove('is-dragover');
                    });
                });
                form.addEventListener('drop', function (e) {
                    droppedFiles = e.dataTransfer.files; // the files that were dropped
                    showFiles(droppedFiles);


                    triggerFormSubmit();

                });
            }

            function UpdateProgressMessage(message) {
                // get result div
                var result = $("#result-message");
                // set message
                result.html(message);
            }

            function UpdateProgressPercent(message) {
                // get result div
                var result = $("#result-percent");
                // set message
                result.html(message + "%");
                result.css("width", message + "%");
            }

            // if the form was submitted
            form.addEventListener('submit', function (e) {
                $("#content").load("/Channel/UploadInfoPartial");

                // preventing the duplicate submissions if the current one is in progress
                if (form.classList.contains('is-uploading')) return false;

                form.classList.add('is-uploading');
                form.classList.remove('is-error');

                if (isAdvancedUpload) // ajax file upload for modern browsers
                {
                    e.preventDefault();

                    // ajax request
                    var ajax = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");

                    ajax.open(form.getAttribute('method'), form.getAttribute('action'), true);
                    ajax.setRequestHeader('Content-type', 'multipart/form-data');
                    ajax.setRequestHeader('X-File-Name', droppedFiles[0].name);
                    ajax.setRequestHeader('X-File-Type', droppedFiles[0].type);
                    ajax.setRequestHeader('X-File-Size', droppedFiles[0].size);
                    ajax.onload = function () {
                        form.classList.remove('is-uploading');
                        if (ajax.status >= 200 && ajax.status < 400) {
                            var data = JSON.parse(ajax.responseText);
                            form.classList.add(data.success == true ? 'is-success' : 'is-error');
                            if (!data.success) errorMsg.textContent = data.error;
                        }
                        else alert('Error. Please, contact the webmaster!');
                    };

                    ajax.onerror = function () {
                        form.classList.remove('is-uploading');
                        alert('Error. Please, try again!');
                    };

                    ajax.send(droppedFiles[0]);

                    var connection = $.hubConnection();
                    var contosoChatHubProxy = connection.createHubProxy('processHub');
                    var videoId;
                    var loggedInUserId = $('#logged_in_user_id').val();

                    contosoChatHubProxy.on('sendMessage', function (message) {
                        //allocate video space in db
                        if (message.indexOf('allocated') > -1) {
                            videoId = message.split(" ")[1];
                            console.log(videoId);
                        }
                        //Show thumbnails
                        if (message.indexOf('getScreenshots') > -1) {
                            $('.upload_info .thumb').load('/Channel/SuggestVideoThumbnails?videoName=' + message.split(" ")[1] + "&videoId=" + videoId);
                        }
                        else {

                            // update progress
                            var percent = message.split(" ")[2];
                            var mess = message.split(" ")[0] + " " + message.split(" ")[1];
                            UpdateProgressPercent(percent);
                            UpdateProgressMessage(mess);
                        }
                        //enable save button
                        if (message.indexOf('Saving Completed') > -1) {
                            $("#save_video").attr('disabled', false);
                            $("#video_link").val(message.split(" ")[3]);
                            $("#video_duration").val(message.split(" ")[4]);
                            $("#video_id").val(videoId);
                            console.log(message.split(" ")[4])
                            return;
                        }
                    });

                    connection.start().done(function () {
                        contosoChatHubProxy.invoke.apply(contosoChatHubProxy, ['uploadOriginal', 'start', loggedInUserId]);
                    }).fail(function (failreason) {
                        alert(failreason);
                    });

                    ajax.onreadystatechange = function () {
                        if (ajax.readyState == 4 && ajax.status == 200) {

                            connection.start().done(function () {
                                contosoChatHubProxy.invoke.apply(contosoChatHubProxy, ['uploadOriginal', 'finished']);
                            }).fail(function (failreason) {
                                alert(failreason);
                            });

                            while (typeof videoId == 'undefined') {

                            }
                            console.log("aa " + videoId);

                            connection.start().done(function () {
                                contosoChatHubProxy.invoke.apply(contosoChatHubProxy, ['saveVideo', droppedFiles[0].name, videoId]);
                            }).fail(function (failreason) {
                                alert(failreason);
                            });
                        }
                    }

                    return false;
                }
                else // fallback Ajax solution upload for older browsers
                {
                    var iframeName = 'uploadiframe' + new Date().getTime(),
						iframe = document.createElement('iframe');

                    $iframe = $('<iframe name="' + iframeName + '" style="display: none;"></iframe>');

                    iframe.setAttribute('name', iframeName);
                    iframe.style.display = 'none';

                    document.body.appendChild(iframe);
                    form.setAttribute('target', iframeName);

                    iframe.addEventListener('load', function () {
                        var data = JSON.parse(iframe.contentDocument.body.innerHTML);
                        form.classList.remove('is-uploading')
                        form.classList.add(data.success == true ? 'is-success' : 'is-error')
                        form.removeAttribute('target');
                        if (!data.success) errorMsg.textContent = data.error;
                        iframe.parentNode.removeChild(iframe);
                    });
                }
            });

            // restart the form if has a state of error/success
            Array.prototype.forEach.call(restart, function (entry) {
                entry.addEventListener('click', function (e) {
                    e.preventDefault();
                    form.classList.remove('is-error', 'is-success');
                    input.click();
                });
            });

            // Firefox focus bug fix for file input
            input.addEventListener('focus', function () { input.classList.add('has-focus'); });
            input.addEventListener('blur', function () { input.classList.remove('has-focus'); });

        });
    }(document, window, 0));



    $('.playlist_items .videos').slick({
        slidesToShow: 4,
        infinite: false,
        prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button"><i class="fa fa-arrow-left"></i></button>',
        nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button"><i class="fa fa-arrow-right"></i></button>',
        responsive: [
            {
                breakpoint: 900,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 500,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    });
});