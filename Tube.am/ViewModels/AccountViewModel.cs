﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Tube.am.ViewModels
{
    public class AccountViewModel
    {
        public class CreateModel : GlobalViewModel
        {
            [Required]
            public string NameSurename { get; set; }
            [Required]
            public string Email { get; set; }
            [Required]
            public string Password { get; set; }
        }

        public class LoginModel : GlobalViewModel
        {
            [Required]
            public string Email { get; set; }
            [Required]
            public string Password { get; set; }

            public string LoginError { get; set; }

            public string RegisterError { get; set; }
        }
    }
}