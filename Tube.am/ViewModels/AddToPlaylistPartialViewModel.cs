﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tube.am.Absract.Models;
using Tube.am.DAL.Managers;

namespace Tube.am.ViewModels
{
    public class AddToPlaylistPartialViewModel
    {
        public VideoModel Video { get; set; }

        public UserModel LoggedInUser { get; set; }

        public List<PlaylistModel> UserPlaylists { get; set; }

        public AddToPlaylistPartialViewModel()
        {
            Video = new VideoModel();
            LoggedInUser = UserModelView.LoggedInUser;

            if (LoggedInUser.Id == null)
                UserPlaylists = new List<PlaylistModel>();
            else
                UserPlaylists = PlaylistManager.GetUserPlaylists(LoggedInUser.Id);
        }
    }
}