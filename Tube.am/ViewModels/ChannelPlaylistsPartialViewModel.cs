﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tube.am.Absract.Models;

namespace Tube.am.ViewModels
{
    public class ChannelPlaylistsPartialViewModel
    {
        public UserModel Channel { get; set; }

        public List<PlaylistModel> Playlists { get; set; }

        public ChannelPlaylistsPartialViewModel()
        {
            Channel = new UserModel();
            Playlists = new List<PlaylistModel>();
        }
    }
}