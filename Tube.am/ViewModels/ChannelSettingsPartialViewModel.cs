﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tube.am.Absract.Models;

namespace Tube.am.ViewModels
{
    public class ChannelSettingsPartialViewModel
    {
        public UserModel Channel { get; set; }

        public ChannelSettingsPartialViewModel()
        {
            Channel = new UserModel();
        }
    }
}