﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tube.am.Absract.Models;

namespace Tube.am.ViewModels
{
    public class ChannelVideosPartialViewModel
    {
        public UserModel Channel { get; set; }

        public List<VideoModel> Videos { get; set; }

        public ChannelVideosPartialViewModel()
        {
            Channel = new UserModel();
            Videos = new List<VideoModel>();
        }
    }
}