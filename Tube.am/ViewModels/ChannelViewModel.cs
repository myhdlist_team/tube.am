﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tube.am.Absract.Models;

namespace Tube.am.ViewModels
{
    public class ChannelViewModel :GlobalViewModel
    {
        public bool MyPage { get; set; }

        public bool LoggedIn { get; set; }

        public bool IsSubscribed { get; set; }

        public int SubscribersCount { get; set; }

        public UserModel Channel { get; set; }

        public List<VideoModel> Videos { get; set; }

        public List<MovieModel> Movies { get; set; }

        public List<PlaylistModel> Playlists { get; set; }

        public ChannelViewModel()
        {
            MyPage = false;
            LoggedIn = false;
            IsSubscribed = false;
            Channel = new UserModel();
            Videos = new List<VideoModel>();
            Movies = new List<MovieModel>();
            Playlists = new List<PlaylistModel>();
        }
    }
}