﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tube.am.Absract.Models;
using Tube.am.DAL.Managers;

namespace Tube.am.ViewModels
{
    public class GlobalViewModel
    {
        public string Page { get; set; }

        public List<PlaylistModel> UserPlaylists { get; set; }

        public Dictionary<UserModel, int> UsersSubscribedChannels { get; set; }

        public GlobalViewModel()
        {
            Page = "";

            if (UserModelView.LoggedInUser.Id == null)
            {
                UserPlaylists = new List<PlaylistModel>();
                UsersSubscribedChannels = new Dictionary<UserModel, int>();
            }
            else
            {
                UserPlaylists = PlaylistManager.GetUserPlaylists(UserModelView.LoggedInUser.Id);
                UsersSubscribedChannels = UserManager.GetUsersSubscribedChannelsWithNewVideoCount(UserModelView.LoggedInUser.Id);
            }
        }
    }
}