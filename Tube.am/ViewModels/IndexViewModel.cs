﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tube.am.Absract.Models;
using Tube.am.DAL.Managers;

namespace Tube.am.ViewModels
{
    public class IndexViewModel : GlobalViewModel
    {
        public List<VideoModel> Top3Videos { get; set; }

        public List<VideoModel> MostViewVideos { get; set; }

        public List<MovieModel> MostViewedMovies { get; set; }

        public List<MovieModel> NewestMovies { get; set; }

        public List<PlaylistModel> PopularPlaylists { get; set; }

        public UserModel SubChannel1 { get; set; }

        public UserModel SubChannel2 { get; set; }

        public UserModel SubChannel3 { get; set; }

        public List<VideoModel> SubChannel1Videos { get; set; }

        public List<VideoModel> SubChannel2Videos { get; set; }

        public List<VideoModel> SubChannel3Videos { get; set; }

        public IndexViewModel()
        {
            Top3Videos = VideoManager.GetTop3Videos();
            MostViewVideos = VideoManager.GetMostViewedVideos();
            MostViewedMovies = MovieManager.GetMostViewedMovies();
            NewestMovies = MovieManager.GetNewestMovies();
            PopularPlaylists = PlaylistManager.GetPopularPlaylists();

            SubChannel1 = null;
            SubChannel2 = null;
            SubChannel3 = null;

            SubChannel1Videos = new List<VideoModel>();
            SubChannel2Videos = new List<VideoModel>();
            SubChannel3Videos = new List<VideoModel>();

            if (UserModelView.LoggedInUser.Id != null)
            {
                List<UserModel> usersSubbedChannels = UserManager.GetUsersSubscribedChannels(UserModelView.LoggedInUser.Id);

                List<UserModel> top3 = UserManager.GetTop3Channels(usersSubbedChannels);

                if (top3.Any())
                {
                    SubChannel1 = top3[0];
                    SubChannel1Videos = VideoManager.GetUserAllVideos(top3[0].Id);

                    if (top3.Count > 1)
                    {
                        SubChannel2 = top3[1];
                        SubChannel2Videos = VideoManager.GetUserAllVideos(top3[1].Id);
                    }
                    if (top3.Count > 2)
                    {
                        SubChannel3 = top3[2];
                        SubChannel3Videos = VideoManager.GetUserAllVideos(top3[2].Id);
                    }
                }
            }

        }

    }
}