﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tube.am.Absract.Models;

namespace Tube.am.ViewModels
{
    public class LikeDislikePartialViewModel
    {
        public VideoModel Video { get; set; }

        public UserModel LoggedInUser { get; set; }

        public LikeDislikePartialViewModel()
        {
            Video = new VideoModel();
            LoggedInUser = UserModelView.LoggedInUser;
        }
    }
}