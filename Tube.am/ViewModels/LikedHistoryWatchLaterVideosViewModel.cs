﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tube.am.Absract.Models;
using Tube.am.DAL.Managers;

namespace Tube.am.ViewModels
{
    public class LikedHistoryWatchLaterVideosViewModel : GlobalViewModel
    {
        public List<VideoModel> Videos { get; set; }

        public LikedHistoryWatchLaterVideosViewModel()
        {
            Videos = new List<VideoModel>();
        }
    }
}