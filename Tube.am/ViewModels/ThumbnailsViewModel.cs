﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Tube.am.ViewModels
{
    public class ThumbnailsViewModel
    {
        public List<string> ThumbnailLinks { get; set; }

        public int VideoId { get; set; }

        public ThumbnailsViewModel()
        {
            ThumbnailLinks = new List<string>();
        }
    }
}