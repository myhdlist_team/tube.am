﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tube.am.Absract.Models;
using Tube.am.DAL.Managers;

namespace Tube.am.ViewModels
{
    public static class UserModelView
    {
        public static UserModel LoggedInUser
        {
            get
            {
                return UserManager.GetUserById(HttpContext.Current.User.Identity.GetUserId());
            }
        }
    }
}
