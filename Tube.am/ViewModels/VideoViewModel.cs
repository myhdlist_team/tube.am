﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tube.am.Absract.Models;

namespace Tube.am.ViewModels
{
    public class VideoViewModel : GlobalViewModel
    {
        public bool MyPage { get; set; }

        public bool LoggedIn { get; set; }

        public bool IsSubscribed { get; set; }

        public VideoModel Video { get; set; }

        public bool IsPlaylist { get; set; }

        public PlaylistModel Playlist { get; set; }

        public List<VideoModel> RelatedVideos { get; set; }

        public VideoViewModel()
        {
            MyPage = false;
            LoggedIn = false;
            IsSubscribed = false;
            Video = new VideoModel();
            RelatedVideos = new List<VideoModel>();
        }
    }
}